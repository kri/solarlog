package ch.kri.richards.SolarLogServer;

import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.logging.Logger;

/**
 * This class provides HTTP access to the data stored in the database.
 */
public class WebService extends Thread {
	private final int port;
	private final Logger logger;

	public WebService(int port, Logger logger) {
		this.port = port;
		this.logger = logger;
	}

	@Override
	public void run() {
		try (ServerSocket listener = new ServerSocket(port, 10, null)) {
			while (true) {				
				Socket socket = listener.accept(); // Wait for request			
				WebServiceClient client = new WebServiceClient(socket, logger); // Serve the client
				client.start();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
