package ch.kri.richards.SolarLogServer.dataClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import ch.kri.richards.SolarLogServer.SolarLogServer;
import ch.kri.richards.SolarLogServer.Utility;

/**
 * Fetching data from the SolarLog via JSON
 * 
 * To get the data for this class, send the query "{"801":{"170":null}}"
 * 
 * To get data for individual inverters, send the query "{"782":null}" --> Refer
 * to SolarLog configuration for the order of the devices
 * 
 * Helpful article:
 * https://www.matusz.ch/blog/2021/10/09/andere-solar-log-daten-auslesen/
 * 
 * SolarLog documentation:
 * https://www.manualslib.de/manual/578264/Solar-Log-Solar-Log-Base.html?page=211#manual
 */

public class DataInstant extends DataInterface {
	// All attributes public, because they are final anyway...
	public final Instant Timestamp;
	public final int PowerAC;
	public final int PowerDC;
	public final int VoltageAC;
	public final int VoltageDC;
	public final int WattHoursToday;
	public final int WattHoursYesterday;
	public final int WattHoursThisMonth;
	public final int WattHoursThisYear;
	public final int WattHoursTotal;
	public final int WattsUsage;
	public final int WattHoursUsageToday;
	public final int WattHoursUsageYesterday;
	public final int WattHoursUsageThisMonth;
	public final int WattHoursUsageThisYear;
	public final int WattHoursUsageTotal;
	public final int InstalledCapacity;
	
	private final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm:ss");
	
	/**
	 * Constructor from JSON (depends on the SolarLog specific format for timestamp)
	 */
	public DataInstant(String httpTimestamp, String json) {
		// We disregard the httpTimestamp and use the one in the data
		String timeAsString = json.substring(22, 39);
		Timestamp = LocalDateTime.parse(timeAsString, format).toInstant(Utility.getZoneOffset());

		// Parse data
		String dataAsString = json.substring(41, json.length() - 3);
		String[] parts = dataAsString.split(",");
		int[] data = new int[16];
		for (int i = 0; i < 16; i++) {
			String[] subparts = parts[i].split(":");
			data[i] = Integer.parseInt(subparts[1]);
		}
		PowerAC = data[0];
		PowerDC = data[1];
		VoltageAC = data[2];
		VoltageDC = data[3];
		WattHoursToday = data[4];
		WattHoursYesterday = data[5];
		WattHoursThisMonth = data[6];
		WattHoursThisYear = data[7];
		WattHoursTotal = data[8];
		WattsUsage = data[9];
		WattHoursUsageToday = data[10];
		WattHoursUsageYesterday = data[11];
		WattHoursUsageThisMonth = data[12];
		WattHoursUsageThisYear = data[13];
		WattHoursUsageTotal = data[14];
		InstalledCapacity = data[15];
	}
	
	/**
	 * Constructor from the database
	 * @throws SQLException 
	 */
	public DataInstant(ResultSet rs) throws SQLException {
		Timestamp = rs.getTimestamp(1).toInstant();
		PowerAC = rs.getInt(2);
		PowerDC = rs.getInt(3);
		VoltageAC = rs.getInt(4);
		VoltageDC = rs.getInt(5);
		WattHoursToday = rs.getInt(6);
		WattHoursYesterday = rs.getInt(7);
		WattHoursThisMonth = rs.getInt(8);
		WattHoursThisYear = rs.getInt(9);
		WattHoursTotal = rs.getInt(10);
		WattsUsage = rs.getInt(11);
		WattHoursUsageToday = rs.getInt(12);
		WattHoursUsageYesterday = rs.getInt(13);
		WattHoursUsageThisMonth = rs.getInt(14);
		WattHoursUsageThisYear = rs.getInt(15);
		WattHoursUsageTotal = rs.getInt(16);
		InstalledCapacity = rs.getInt(17);
	}

	@Override
	public String toString() {
		return String.format(
				"Timestamp: %s\nPowerAC: %d\nPowerDC: %d\nVoltageAC: %d\nVoltageDC: %d\nWattHoursToday: %d\n"
						+ "WattHoursYesterday: %d\nWattHoursThisMonth: %d\nWattHoursThisYear: %d\nWattHoursTotal: %d\n"
						+ "WattsUsage: %d\nWattHoursUsageToday: %d\nWattHoursUsageYesterday: %d\nWattHoursUsageThisMonth: %d\n"
						+ "WattHoursUsageThisYear: %d\nWattHoursUsageTotal: %d\n",
				Timestamp, PowerAC, PowerDC, VoltageAC, VoltageDC, WattHoursToday, WattHoursYesterday,
				WattHoursThisMonth, WattHoursThisYear, WattHoursTotal, WattsUsage, WattHoursUsageToday,
				WattHoursUsageYesterday, WattHoursUsageThisMonth, WattHoursUsageThisYear, WattHoursUsageTotal,
				InstalledCapacity);
	}

	@Override
	public void toDatabase(Connection cn) {
		try {
			String query = RequestType.MINUTE_DATA.getInsertQuery();
			PreparedStatement stmt = cn.prepareStatement(query);
			Timestamp ts = java.sql.Timestamp.from(this.Timestamp);
			stmt.setTimestamp(1, ts);
			stmt.setInt(2, PowerAC);
			stmt.setInt(3, PowerDC);
			stmt.setInt(4, VoltageAC);
			stmt.setInt(5, VoltageDC);
			stmt.setInt(6, WattHoursToday);
			stmt.setInt(7, WattHoursYesterday);
			stmt.setInt(8, WattHoursThisMonth);
			stmt.setInt(9, WattHoursThisYear);
			stmt.setInt(10, WattHoursTotal);
			stmt.setInt(11, WattsUsage);
			stmt.setInt(12, WattHoursUsageToday);
			stmt.setInt(13, WattHoursUsageYesterday);
			stmt.setInt(14, WattHoursUsageThisMonth);
			stmt.setInt(15, WattHoursUsageThisYear);
			stmt.setInt(16, WattHoursUsageTotal);
			stmt.setInt(17, InstalledCapacity);
			stmt.execute();
		} catch (SQLException e) {
			SolarLogServer.logger.warning("Unable to save MINUTE_DATA to database: " + e.getMessage());
		}
	}

	/**
	 * Format this object as JSON. This means enclosed in {} brackets, attributes
	 * separated by commas, Attribute-names and values formatted as strings. For the
	 */
	@Override
	public String toJSON() {
		return "{" + Utility.jsonQuote("Timestamp", DateTimeFormatter.ISO_INSTANT.format(Timestamp)) + ","
				+ Utility.jsonQuote("PowerAC", PowerAC) + ", "
				+ Utility.jsonQuote("PowerDC", PowerDC) + ","
				+ Utility.jsonQuote("VoltageAC", VoltageAC) + ","
				+ Utility.jsonQuote("VoltageDC", VoltageDC) + ","
				+ Utility.jsonQuote("WattHoursToday", WattHoursToday) + ","
				+ Utility.jsonQuote("WattHoursYesterday", WattHoursYesterday) + ","
				+ Utility.jsonQuote("WattHoursThisMonth", WattHoursThisMonth) + ","
				+ Utility.jsonQuote("WattHoursThisYear", WattHoursThisYear) + ","
				+ Utility.jsonQuote("WattHoursTotal", WattHoursTotal) + ","
				+ Utility.jsonQuote("WattsUsage", WattsUsage) + ","
				+ Utility.jsonQuote("WattHoursUsageToday", WattHoursUsageToday) + "," 
				+ Utility.jsonQuote("WattHoursUsageYesterday", WattHoursUsageYesterday) + ","
				+ Utility.jsonQuote("WattHoursUsageThisMonth", WattHoursUsageThisMonth) + ","
				+ Utility.jsonQuote("WattHoursUsageThisYear", WattHoursUsageThisYear) + ","
				+ Utility.jsonQuote("WattHoursUsageTotal", WattHoursUsageTotal) + ","
				+ Utility.jsonQuote("InstalledCapacity", InstalledCapacity) + "}";
	}
}
