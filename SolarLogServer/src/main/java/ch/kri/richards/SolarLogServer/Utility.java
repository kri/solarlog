package ch.kri.richards.SolarLogServer;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;

import ch.kri.richards.SolarLogServer.dataClasses.DataInterface;

public class Utility {


	/**
	 * Helper method to quote standard data-types
	 */
	public static String jsonQuote(String name, Object value) {
		return String.format("\"%s\":\"%s\"", name, value);
	}
	
	/**
	 * Helper method to turn an ArrayList into a JSON-array
	 */
	public static String jsonArray(ArrayList<? extends DataInterface> objects) {
		StringBuffer buf = new StringBuffer();
		buf.append("[");
		for (int i = 0; i < objects.size()-1; i++) {
			buf.append(objects.get(i).toJSON());
			buf.append(", ");
		}
		buf.append(objects.get(objects.size()-1).toJSON());
		buf.append("]");
		return buf.toString();
	}
	
	/**
	 * Return the current timezone offset for where we are located
	 * (See https://www.baeldung.com/java-zone-offset)
	 */
	public static ZoneOffset getZoneOffset() {
		LocalDateTime now = LocalDateTime.now();
		ZoneId zone = ZoneId.of("Europe/Zurich");
		return zone.getRules().getOffset(now);
	}
}
