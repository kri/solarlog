package ch.kri.richards.SolarLogServer.dataClasses;

/**
 * This enumeration define, for each data type, the database table name and
 * the class name used to represent it.
 */
public enum RequestType {
	DEVICE_DATA("tblDeviceData", DeviceInstant.class, "{\"782\":null}", "Timestamp", "Meter", "Switch", "Inverter", "ChargingStation"),
	MINUTE_DATA("tblMinuteData", DataInstant.class, "{\"801\":{\"170\":null}}", "Timestamp", "PowerAC", "PowerDC", "VoltageAC", "VoltageDC", "WattHoursToday", "WattHoursYesterday", "WattHoursThisMonth", "WattHoursThisYear", "WattHoursTotal", "WattsUsage", "WattHoursUsageToday", "WattHoursUsageYesterday", "WattHoursUsageThisMonth", "WattHoursUsageThisYear", "WattHoursUsageTotal", "InstalledCapacity");
	
	public final String databaseTableName;
	public final Class<? extends DataInterface> clazz;
	public final String solarLogQuery; // JSON query to send to SolarLog
	public final String[] fieldList; // List of database fields, in order
	
	private RequestType(String databaseTableName, Class<? extends DataInterface> clazz, String query, String... fields) {
		this.databaseTableName = databaseTableName;
		this.clazz = clazz;
		this.solarLogQuery = query;
		this.fieldList = fields;
	}
	
	private String getFieldsAsString() {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < fieldList.length; i++) {
			buf.append(fieldList[i]);
			if (i < fieldList.length-1) buf.append(", ");
		}
		return buf.toString();
	}
	
	public String getInsertQuery() {
		return "INSERT INTO " + databaseTableName + " (" + getFieldsAsString() + ") VALUES (" + getQuestionMarks() + ")";
	}
	
	/**
	 * For PreparedStatements, we need placeholders in the form of (?, ?, ?...?)
	 * 
	 * This method constructs a String with the desired number of question-marks
	 */
	private String getQuestionMarks() {
		StringBuffer buf = new StringBuffer();
		for (int i = 1; i < fieldList.length; i++) buf.append("?, ");
		buf.append("?");
		return buf.toString();
	}
	
}
