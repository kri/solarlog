package ch.kri.richards.SolarLogServer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import ch.kri.richards.SolarLogServer.dataClasses.DataInterface;
import ch.kri.richards.SolarLogServer.dataClasses.RequestType;

public class Updater {
	private final Logger logger;
	private final String solarLogIP;
//	private Date lastDayData = new Date(0);
//	private Date lastMonthData = new Date(0);
	private Runnable minuteDataUpdater;
	private Runnable deviceDataUpdater;
	private Thread minuteDataThread;
	private Thread deviceDataThread;

	public Updater(Logger logger, String solarLogIP) {
		this.logger = logger;
		this.solarLogIP = solarLogIP;
		minuteDataUpdater = () -> {
			dataUpdater(RequestType.MINUTE_DATA);
		};
		deviceDataUpdater = () -> {
			dataUpdater(RequestType.DEVICE_DATA);
		};
	}

	public void start() {
		minuteDataThread = new Thread(minuteDataUpdater, "Minute data updater");
		deviceDataThread = new Thread(deviceDataUpdater, "Device data updater");
		minuteDataThread.start();
		deviceDataThread.start();
	}

	public void stop() {
		minuteDataThread.interrupt();
		deviceDataThread.interrupt();
	}

	// Method used for updating various data types; details taken from the enumeration
	private void dataUpdater(RequestType type) {
		logger.fine("Starting update-thread for " + type);
		while (true) {
			DataInterface dataInstant = null;

			// Read values from the SolarLog
			boolean readSuccess = false;
			while (!readSuccess) {
				logger.fine("Begin HTTP query for " + type);
				String lineIn;
				try (Socket s = new Socket(solarLogIP, 80);
						OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
						BufferedReader inReader = new BufferedReader(new InputStreamReader(s.getInputStream()));) {
					s.setSoTimeout(90000); // Normal delay is 60s; we wait 90s
					String jsonResponse = null;
					String timestamp = null;
					// Send our request, using the HTTP 1.0 protocol
					// Note: HTTP specifies \r\n line endings, though most programs don't care
					out.write("POST /getjp HTTP/1.1\r\n");
					out.write("Host: " + solarLogIP + "\r\n");
					out.write("Content-Length: " + type.solarLogQuery.length() + "\r\n\r\n"); // Blank line at the end of headers!!!
					out.write(type.solarLogQuery + "\r\n"); // Content of POST query
					out.flush();
	
					// Start reading, looking for the HTTP timestamp and the single line of json output
					// Note that the SolarLog delays its response by 60 seconds
					while ((lineIn = inReader.readLine()) != null) {
						if (timestamp == null && !lineIn.isEmpty() && lineIn.startsWith("Date:")) {
							timestamp = lineIn.substring(6);
						}
						if (jsonResponse == null && !lineIn.isEmpty() && lineIn.charAt(0) == '{') {
							jsonResponse = lineIn;
						}
					}
					readSuccess = true;
					logger.fine("HTTP success for " + type + "\n" + jsonResponse);
					
					// Use reflection to get the correct class and call the constructor
					Class<? extends DataInterface> clazz = type.clazz;
					Constructor<? extends DataInterface> constructor = clazz.getConstructor(String.class, String.class);
					dataInstant = constructor.newInstance(timestamp, jsonResponse);
				} catch (Exception e) { // Any and all exceptions, including timeout
					logger.warning("HTTP failure for " + type + ": " + e.getMessage());
					// Wait a bit before retrying...
					try { Thread.sleep(30000); } catch (InterruptedException e1) { }
				}
			}

			// Write values to the database
			if (dataInstant != null) {
				try {
					Connection cn = DatabaseConnection.getDatabaseConnection(logger).getConnection();
					logger.fine("Database write for " + type + "\n" + dataInstant.toString());
					dataInstant.toDatabase(cn);
					logger.fine("Database write success for " + type);
				} catch (SQLException e) {
					logger.severe("Unable to write to database");
				}
			} else {
				logger.warning("Data instant was null for " + type);				
			}
			
			// The SolarLog delays 60 seconds before answering. In case this delay
			// ever goes away, we have a small delay here, so we don't bombard it
			try { Thread.sleep(1000); } catch (InterruptedException e1) { }
		}
	}
}
