package ch.kri.richards.SolarLogServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class DatabaseConnection {
	private static final String serverInfo = "jdbc:mysql://192.168.1.9:3306/solarlog";
	private static final String optionInfo = "?connectTimeout=3000";
	private static final String username = "solarlog";
	private static final String password = "solarlog";
	private static final int retries = 7;
	private static int retryDelay = 5000; // initial delay in ms

	private static DatabaseConnection singleton;

	private Logger logger;
	private Connection cn;;

	public static DatabaseConnection getDatabaseConnection(Logger logger) throws SQLException {
		if (singleton == null)
			singleton = new DatabaseConnection(logger);
		return singleton;
	}

	private DatabaseConnection(Logger logger) throws SQLException {
		this.logger = logger;
		this.cn = getConnection();
	}

	public Connection getConnection() throws SQLException {
		if (cn == null || !cn.isValid(5)) {
			int failedTries = 0;
			cn = null;
			while (failedTries < retries && cn == null) {
				try {
					logger.info("Database connection attempt");
					cn = DriverManager.getConnection(serverInfo + optionInfo, username, password);
				} catch (SQLException e) {
					logger.warning("Database connection attempt failed: " + e.getMessage());
					cn = null;
				}
				if (cn == null) {
					failedTries++;
					try {
						Thread.sleep(retryDelay);
					} catch (InterruptedException e) {
					}
					retryDelay *= 2;
				}
			}
		}
		if (cn == null) {
			logger.severe("Unable to create database connection");
			throw new SQLException("Unable to create database connection");
		}
		logger.info("Connected to database");

		return cn;
	}
}
