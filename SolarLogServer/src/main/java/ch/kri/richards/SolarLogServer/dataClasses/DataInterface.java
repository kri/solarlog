package ch.kri.richards.SolarLogServer.dataClasses;

import java.sql.Connection;

/**
 * Require methods for converting to SQL and JSON
 */
public abstract class DataInterface {
	// Required output methods
	public abstract void toDatabase(Connection cn);
	public abstract String toJSON();
}
