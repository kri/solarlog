package ch.kri.richards.SolarLogServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Logger;

import ch.kri.richards.SolarLogServer.dataClasses.DataInterface;
import ch.kri.richards.SolarLogServer.dataClasses.RequestType;

/**
 * We only accept GET requests of the form /dataType/date
 * 
 * For example: GET /deviceData/2023-04-26 is a request for device-data on 26 April 2023
 * 
 * The initial version of this class supports minuteData and deviceData
 * 
 * Any non-conforming requests will be answered with status 404, and no further information
 */
public class WebServiceClient extends Thread {
	private Socket socket;
	private Logger logger;

	public WebServiceClient(Socket socket, Logger logger) {
		this.socket = socket;
		this.logger = logger;
	}

	// Request information
	private record Request(RequestType requestType, LocalDate requestDate) {}
	
	@Override
	public void run() {
		// Create input and output streams to talk to the client
		try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				OutputStream outBinary = socket.getOutputStream();
				PrintWriter outText = new PrintWriter(outBinary);) {
			logger.fine("WebClient request received");
			// Read request from client
			// An empty string (length 0) is the end of an HTTP request
			StringBuilder received = new StringBuilder();
			String inString;
			// Crude prevention against attack: limit max request size to around 10000 characters.
			// Doesn't help, of course, if it's all one huge line.
			while ((inString = in.readLine()) != null && inString.length() != 0 && received.length() < 10000) {
				received.append(inString + "\n");
			}

			// Parse the file name: out method returns something like webroot/file.html
			Request request = parseGetRequest(received.toString());

			String data = null;
			if (request != null) {
				logger.fine("WebClient request: " + request);
				// Request looks valid - try to fetch some data
				try {
					data = getJSON(request);
				} catch (SQLException e) {
					logger.fine("WebClient: getJSON failed");
					// Do nothing; request failed
				}
			}
			
			// Valid request?
			if (request == null || data == null) {
				// No good, status 404
				outText.print("HTTP/1.0 404 \r\n"); // Version and status
				outText.print("Content-Type: text/plain\r\n");
				outText.print("\r\n"); // Blank line ends the HTTP protocol
				outText.print("Invalid request\n"); // Content of reply
			} else {
				// All ok, status 200
				outText.print("HTTP/1.0 200 \r\n"); // Version and status
				outText.print("Content-Type: application/json\r\n"); // media type
				outText.print("\r\n"); // Blank line ends the HTTP protocol
				
				// Send the data
				outText.print(data);
				outText.print("\r\n");
			}
			outText.flush(); // Be safe, always "flush"
			socket.close();
		} catch (IOException e) {
			// TODO - ignore for now
		}
	}

	/**
	 * Examine the incoming request. If it is a GET request, locate and return the
	 * requested file name. If the request is not a GET request, or if we have any
	 * problems, return null.
	 * 
	 * If the root of our web content is a subdirectory "www" underneath the working
	 * directory, then a request for "woof.html" will become "www/woof.html", and a
	 * request for "/path/to/woof.html" will become "www/path/to/woof.html".
	 * 
	 * @param in
	 *            Incoming request read from the client
	 * @return Name of the requested file, or else null
	 */
	private Request parseGetRequest(String in) {
		Request request = null;

		if (in.regionMatches(0, "GET ", 0, 4)) {
			int fileNameEnd = in.indexOf(" ", 4);
			String requestContent = in.substring(4, fileNameEnd).trim();

			// Sanitize to prevent hacking
			requestContent = requestContent.replaceAll("[^0-9_/a-zA-Z\\-\\.]", "");
			if (requestContent.length() >= 13 && requestContent.length() <= 25 && requestContent.charAt(0) == '/') {
				String[] parts = requestContent.split("/");
				if (parts.length == 3) {
					try {
						RequestType rt = RequestType.valueOf(parts[1]);
						LocalDate d = LocalDate.parse(parts[2], DateTimeFormatter.ISO_LOCAL_DATE);
						request = new Request(rt, d);
					} catch (Exception e) {
						// We don't care why we failed...
					}
				}
			}
		}
		return request;
	}
	
	private String getJSON(Request request) throws SQLException {
		String dateString = request.requestDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
		Connection cn = DatabaseConnection.getDatabaseConnection(logger).getConnection();
		// Construct query
		String query = "SELECT * FROM " + request.requestType.databaseTableName + " WHERE Timestamp BETWEEN '" + dateString + " 00:00:00' AND '" + dateString + " 23:59:59' ORDER BY Timestamp ASC";
		PreparedStatement stmt = cn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY,
				ResultSet.CONCUR_READ_ONLY);
		
		// Get ResultSet and create Java objects
		ArrayList<DataInterface> objects = new ArrayList<>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			// Use reflection to get the correct class and call the constructor
			Class<? extends DataInterface> clazz = request.requestType.clazz;
			try {
				Constructor<? extends DataInterface> constructor = clazz.getConstructor(ResultSet.class);
				DataInterface dataObject = constructor.newInstance(rs);
				objects.add(dataObject);
			} catch (Exception e) {
				// Lots of possible exceptions, none of which should ever happen.
				throw new SQLException("Reflection problem in WebServiceClient");
			}
		}
		rs.close();
		
		return Utility.jsonArray(objects);
	}
}
