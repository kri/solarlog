package ch.kri.richards.SolarLogServer.dataClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import ch.kri.richards.SolarLogServer.SolarLogServer;
import ch.kri.richards.SolarLogServer.Utility;

/**
 * Fetching data from the SolarLog via JSON
 * 
 * Query devices using "{"782":null}"
 * 
 * The devices are currently in the order:
 * 
 * 0 - Meter
 * 
 * 1 - Switch
 * 
 * 2 - Inverter
 * 
 * 3 - ChargingStation
 * 
 * All further entries imported from JSON are unused and will have the value 0
 * 
 * The JSON does not include a timestamp, so this must be provided separately
 */
public class DeviceInstant extends DataInterface {
	public final Instant Timestamp;
	public final int Meter;
	public final int Switch;
	public final int Inverter;
	public final int ChargingStation;

	/**
	 * Constructor from JSON, taking HTTP timestamp
	 */
	public DeviceInstant(String timestamp, String json) {
		this.Timestamp = LocalDateTime.parse(timestamp, DateTimeFormatter.RFC_1123_DATE_TIME).toInstant(Utility.getZoneOffset());

		String[] parts = json.split("\\{");
		parts = parts[2].split("\\}");
		parts = parts[0].split(",");

		// Raw value import from json
		Map<Integer, Integer> deviceMapping = new HashMap<>();
		for (String part : parts) {
			String[] devInfo = part.split(":");
			Integer id = Integer.parseInt(devInfo[0].substring(1, devInfo[0].length() - 1));
			Integer value = Integer.parseInt(devInfo[1].substring(1, devInfo[1].length() - 1));
			deviceMapping.put(id, value);
		}

		Meter = deviceMapping.get(0);
		Switch = deviceMapping.get(1);
		Inverter = deviceMapping.get(2);
		ChargingStation = deviceMapping.get(3);
	}

	/**
	 * Constructor from the database
	 */
	public DeviceInstant(ResultSet rs) throws SQLException {
		Timestamp = rs.getTimestamp(1).toInstant();
		Meter = rs.getInt(2);
		Switch = rs.getInt(3);
		Inverter = rs.getInt(4);
		ChargingStation = rs.getInt(5);
	}

	@Override
	public String toString() {
		return String.format("Timestamp: %s\nMeter: %d\nSwitch: %d\nInverter: %d\nChargingStation: %d\n", Timestamp,
				Meter, Switch, Inverter, ChargingStation);
	}

	@Override
	public void toDatabase(Connection cn) {
		try {
			String query = RequestType.DEVICE_DATA.getInsertQuery();
			PreparedStatement stmt = cn.prepareStatement(query);
			Timestamp ts = java.sql.Timestamp.from(this.Timestamp);
			stmt.setTimestamp(1, ts);
			stmt.setInt(2, Meter);
			stmt.setInt(3, Switch);
			stmt.setInt(4, Inverter);
			stmt.setInt(5, ChargingStation);
			stmt.execute();
		} catch (SQLException e) {
			SolarLogServer.logger.warning("Unable to save DeviceInstant data to database: " + e.getMessage());
		}
	}

	/**
	 * Format this object as JSON. This means enclosed in {} brackets, attributes
	 * separated by commas, Attribute-names and values formatted as strings. For the
	 */
	@Override
	public String toJSON() {
		return "{" + Utility.jsonQuote("Timestamp", DateTimeFormatter.ISO_INSTANT.format(Timestamp)) + ","
				+ Utility.jsonQuote("Meter", Meter) + ", " + Utility.jsonQuote("Switch", Switch) + ","
				+ Utility.jsonQuote("Inverter", Inverter) + "," + Utility.jsonQuote("ChargingStation", ChargingStation)
				+ "}";
	}
}
