package ch.kri.richards.SolarLogServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SolarLogServer {
	private static final String solarLogIP = "192.168.1.6";

	private static final int PORT = 50005;

	private static final String LOGGER_NAME = SolarLogServer.class.getSimpleName();
	public static final Logger logger = initLogger();

	public static void main(String[] args) throws SQLException {
		DatabaseConnection dbc = DatabaseConnection.getDatabaseConnection(logger); // Connect to the database

		// Start updating the database
		Updater up = new Updater(logger, solarLogIP);
		up.start();

		// Start offering data via HTTP
		WebService ws = new WebService(PORT, logger);
		ws.start();
	}

	private static Logger initLogger() {
		Logger logger = Logger.getLogger(LOGGER_NAME);
		logger.setLevel(Level.FINE);

		// By default there is one handler: the console
		Handler[] defaultHandlers = Logger.getLogger("").getHandlers();
		if (defaultHandlers.length == 1) {
			defaultHandlers[0].setLevel(Level.WARNING);
		} else {
			throw new RuntimeException("More than one default handler found");
		}

		// Add a file handler: put rotating files in the tmp directory
		try {
			// %u is for file-conflicts; this happens if multiple instances
			// of the program are running. %g is the rotating logfile number
			Handler logHandler = new FileHandler("%h/" + LOGGER_NAME + "_%u" + "_%g" + ".log", 1000000, 5);
			logHandler.setLevel(Level.FINE);
			logger.addHandler(logHandler);
		} catch (Exception e) {
			throw new RuntimeException("Unable to initialize log files: " + e.toString());
		}
		return logger;
	}

}
