package ch.kri.richards.SolarLogParsing.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.scene.control.Alert;

public class Model {
	private List<String> inputLines;
	private TreeSet<DayData> dayData = new TreeSet<>();
	private TreeSet<MonthData> monthData = new TreeSet<>();

	public Model() {
		// TODO
	}

	/**
	 * In principle, multiple files can be read, in any order, to create the total data set. We only
	 * parse the daily and monthly data.
	 */
	public boolean readFile(File inputFile) {
		try {
			Path path = Path.of(inputFile.getPath());
			try (Stream<String> stream = Files.lines(path)) {
				inputLines = stream.collect(Collectors.toList());
			}

			int cursor = 0;

			// Find daily data
			while (inputLines.size() > cursor && !inputLines.get(cursor).startsWith("#D")) {
				cursor++;
			}
			// The current line should now be the day header
			if (inputLines.size() <= cursor || !inputLines.get(cursor).startsWith("#D"))
				throw new IOException("No day-header found");

			cursor++; // Move to first data line after header
			while (inputLines.size() > cursor && inputLines.get(cursor).charAt(0) != '#') {
				DayData nextData = DayData.parse(inputLines.get(cursor++));
				if (nextData != null) dayData.add(nextData);
			}

			// The current line should now be the month header
			if (inputLines.size() <= cursor || !inputLines.get(cursor).startsWith("#M"))
				throw new IOException("No month-header found");

			cursor++; // Move to first data line after header
			while (inputLines.size() > cursor && inputLines.get(cursor).charAt(0) != '#') {
				MonthData nextData = MonthData.parse(inputLines.get(cursor++));
				if (nextData != null) monthData.add(nextData);
			}

			// The current line should now be the year header
			if (inputLines.size() <= cursor || !inputLines.get(cursor).startsWith("#Y"))
				throw new IOException("No year-header found");
			
			return true;
		} catch (IOException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
			alert.showAndWait();
			return false;
		}
	}
	
	public void writeFile(String outputPath) {
		try {
			// Output daily data
			String dailyPath = outputPath.replace("XXX", "Daily");
			File dailyFile = new File(dailyPath);
			dailyFile.createNewFile();
			if (dailyFile.exists() && dailyFile.isFile() && dailyFile.canWrite()) {
				BufferedWriter out = new BufferedWriter(new FileWriter(dailyFile));
				out.write("Date;Power Use;Net Use; Production; Production Use; Production Sold\n");
				for (DayData data : dayData) {
					out.write(data.toCSV());
				}
				out.flush();
				out.close();
			} else {
				throw new IOException("Could not open output file for daily data");
			}
			
			// Output monthly data
			String monthlyPath = outputPath.replace("XXX", "Monthly");
			File monthlyFile = new File(monthlyPath);
			monthlyFile.createNewFile();
			if (monthlyFile.exists() && monthlyFile.isFile() && monthlyFile.canWrite()) {
				BufferedWriter out = new BufferedWriter(new FileWriter(monthlyFile));
				out.write("Date;Power Use;Net Use; Production; Production Use; Production Sold\n");
				for (MonthData data : monthData) {
					out.write(data.toCSV());
				}
				out.flush();
				out.close();
			} else {
				throw new IOException("Could not open output file for daily data");
			}
			
		} catch (IOException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
			alert.showAndWait();
		}
	}
}
