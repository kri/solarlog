package ch.kri.richards.SolarLogParsing;

import ch.kri.richards.SolarLogParsing.controller.Controller;
import ch.kri.richards.SolarLogParsing.model.Model;
import ch.kri.richards.SolarLogParsing.view.View;
import javafx.application.Application;
import javafx.stage.Stage;

public class SolarLogParsing extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		Model model = new Model();
		View view = new View(stage, model);
		new Controller(model, view);
		view.start();
	}

}
