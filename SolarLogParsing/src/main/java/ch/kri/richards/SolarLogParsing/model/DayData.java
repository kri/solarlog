package ch.kri.richards.SolarLogParsing.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DayData implements Comparable<DayData> {
	// First valid data is on 7 May 2021
	private static final LocalDate firstValid = LocalDate.of(2021, 05, 07);
	private static final LocalDate carChargerInstalled = LocalDate.of(2022, 03, 21);
	
	private final LocalDate date; // ID
	private final int powerUse; // Total power usage
	private final int netUse; // Power drawn from the net
	private final int production; // Power produced
	private final int productionUse; // Power used from production
	private final int productionSold; // Power sold to the net
	
	private DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE;

	public static DayData parse(String line) {
		String[] parts = line.split(";");
		String[] dateParts = parts[2].split("\\.");
		
		LocalDate date = LocalDate.of(2000 + Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[0]));
		if (date.compareTo(firstValid) >= 0)
			return new DayData(date, parts);
		else
			return null;
	}
	
	private DayData(LocalDate date, String[] parts) {
		this.date = date;		
		powerUse = Integer.parseInt(parts[6]);
		production = Integer.parseInt(parts[12]);
		if (date.compareTo(carChargerInstalled) < 0)
			productionUse = Integer.parseInt(parts[15]);
		else
			productionUse = Integer.parseInt(parts[18]);
		netUse = powerUse - productionUse;
		productionSold = production - productionUse;
	}
	
	public String toCSV() {
		return String.format("%s;%d;%d;%d;%d;%d%n", date.format(dateFormatter), powerUse, netUse, production, productionUse, productionSold);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && this.getClass().equals(o.getClass())) {
			DayData d = (DayData) o;
			return this.date.equals(d.date);
		}
		return false;
	}
	
	@Override
	public int compareTo(DayData d) {
		return this.date.compareTo(d.date);
	}

	// Getters (no setters)
	public LocalDate getDate() {
		return date;
	}

	public int getPowerUse() {
		return powerUse;
	}

	public int getNetUse() {
		return netUse;
	}

	public int getProduction() {
		return production;
	}

	public int getProductionUse() {
		return productionUse;
	}
	
	public int getProductionSold() {
		return productionSold;
	}
}
