package ch.kri.richards.SolarLogParsing.model;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class MonthData implements Comparable<MonthData> {
	// First valid data is on 7 May 2021
	private static final YearMonth firstValid = YearMonth.of(2021, 06);
	
	private final YearMonth date; // ID
	private final int powerUse; // Total power usage
	private final int netUse; // Power drawn from the net
	private final int production; // Power produced
	private final int productionUse; // Power used from production
	private final int productionSold; // Power sold to the net
	
	private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MMM");
	private static final YearMonth carChargerInstalled = YearMonth.of(2022, 04);
	
	public static MonthData parse(String line) {
		String[] parts = line.split(";");
		
		String[] dateParts = parts[2].split("\\.");
		YearMonth date = YearMonth.of(2000 + Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]));
		if (date.compareTo(firstValid) >= 0)
			return new MonthData(date, parts);
		else
			return null;
	}
	
	private MonthData(YearMonth date, String[] parts) {
		this.date = date;
		powerUse = Integer.parseInt(parts[6]);
		production = Integer.parseInt(parts[12]);
		if (date.compareTo(carChargerInstalled) < 0)
			productionUse = Integer.parseInt(parts[15]) * 1000;
		else
			productionUse = Integer.parseInt(parts[18]) * 1000;
		netUse = powerUse - productionUse;
		productionSold = production - productionUse;
	}
	
	public String toCSV() {
		return String.format("%s;%d;%d;%d;%d;%d%n", date.format(dateFormatter), powerUse, netUse, production, productionUse, productionSold);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && this.getClass().equals(o.getClass())) {
			MonthData d = (MonthData) o;
			return this.date.equals(d.date);
		}
		return false;
	}
	
	@Override
	public int compareTo(MonthData d) {
		return this.date.compareTo(d.date);
	}

	// Getters (no setters)
	public YearMonth getDate() {
		return date;
	}

	public int getPowerUse() {
		return powerUse;
	}

	public int getNetUse() {
		return netUse;
	}

	public int getProduction() {
		return production;
	}

	public int getProductionUse() {
		return productionUse;
	}
	
	public int getProductionSold() {
		return productionSold;
	}
}