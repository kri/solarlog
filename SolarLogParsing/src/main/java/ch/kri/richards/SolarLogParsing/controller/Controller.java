package ch.kri.richards.SolarLogParsing.controller;

import java.io.File;

import ch.kri.richards.SolarLogParsing.model.Model;
import ch.kri.richards.SolarLogParsing.view.View;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class Controller {
	Model model;
	View view;

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;

		view.btnInputFile.setOnAction(e -> {
			getInputFile();
		});
	}

	private void getInputFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Input file");
		fileChooser.setTitle("Open SolarLog data file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("SolarLog data files", "*.dat"));
		File inputFile = fileChooser.showOpenDialog(view.stage);
		if (inputFile != null && inputFile.isFile() && inputFile.canRead()) {
			if (model.readFile(inputFile)) {
				String outputPath = inputFile.getPath().replace(".dat", "_XXX.csv");
				model.writeFile(outputPath);
			}
		}
	}
}
