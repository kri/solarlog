package ch.kri.richards.SolarLogParsing.view;


import ch.kri.richards.SolarLogParsing.model.Model;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class View {
	public Stage stage;
	private Model model;
	
	public Label helloWorld = new Label("Hello, World!");
	public Button btnInputFile = new Button("Select input file");
	
	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;
		
		BorderPane root = new BorderPane();
		root.getStyleClass().add("root");
		root.setCenter(btnInputFile);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("slp.css").toExternalForm());
		stage.setTitle("SolarLog Parser");
		stage.setScene(scene);
	}
	
	public void start() {
		stage.show();
	}
}
