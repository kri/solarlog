module ch.kri.richards.SolarLogParsing {
    requires javafx.controls;
	requires transitive javafx.graphics;
	requires transitive javafx.base;
    exports ch.kri.richards.SolarLogParsing;
}
