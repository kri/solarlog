# SolarLogParsing

## Parse SolarLog data files

This application reads data files exported by a SolarLog 1200. It creates CSV files suitable for import into a spreadsheet. Ultimately, it may write the data into a database.
