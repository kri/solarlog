package ch.kri.richards.KebaBrowser;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

public class BrowserModel {
	private final int T_UDP_PAUSE = 100;
	private final int TIMEOUT = 5000;
	
	public String browse(String ipAddress, Integer port) {
		DatagramSocket s = null;
		InetAddress inetAddress = null;
		StringBuilder sb = new StringBuilder();
		try {
			s = new DatagramSocket(7090);
			s.setSoTimeout(TIMEOUT);
			inetAddress = InetAddress.getByName(ipAddress);
			
			sb.append(sendReceive(inetAddress, port, s, "i"));
			sb.append(sendReceive(inetAddress, port, s, "report 1"));
			sb.append(sendReceive(inetAddress, port, s, "report 2"));
			sb.append(sendReceive(inetAddress, port, s, "report 3"));
			sb.append("\n----- END -----");
		} catch (Exception e) {
			sb.append(e.getMessage());
		}		
		return sb.toString();
	}
	
	// Send packet, get an answer
	private String sendReceive(InetAddress inetAddress, int port, DatagramSocket s, String query) {
		DatagramPacket packetIn = new DatagramPacket(new byte[10000], 10000);
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("\n----- %s -----\n", query));
		boolean success = false;
		while (!success) {
			byte[] msg = query.getBytes();
			DatagramPacket packet = new DatagramPacket(msg, msg.length, inetAddress, port);
		
			try {
				s.send(packet);
				s.receive(packetIn);
				sb.append(new String(packetIn.getData(), 0, packetIn.getLength()));
				sb.append("\n");
				success = true;
			} catch (SocketTimeoutException e) {
				sb.append("Timeout on report 1\n");
			} catch (IOException e) {
				sb.append(e.getMessage());
			}
		}
		try {Thread.sleep(T_UDP_PAUSE); } catch (InterruptedException e) {}
		return sb.toString();
	}
}
