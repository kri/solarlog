package ch.kri.richards.SolarLogServerTestProgram;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Fetching data from the SolarLog via JSON
 * 
 * To get the data for this class, send the query "{"801":{"170":null}}"
 * 
 * To get data for individual inverters, send the query "{"782":null}"
 * --> Refer to SolarLog configuration for the order of the devices
 * 
 * Helpful article: https://www.matusz.ch/blog/2021/10/09/andere-solar-log-daten-auslesen/
 * 
 * SolarLog documentation: https://www.manualslib.de/manual/578264/Solar-Log-Solar-Log-Base.html?page=211#manual
 */

public class DataInstant {
	// All attributes public, because they are final anyway...
	public final LocalDateTime timestamp;
	public final int PowerAC;
	public final int PowerDC;
	public final int VoltageAC;
	public final int VoltageDC;
	public final int WattHoursToday;
	public final int WattHoursYesterday;
	public final int WattHoursThisMonth;
	public final int WattHoursThisYear;
	public final int WattHoursTotal;
	public final int WattsUsage;
	public final int WattHoursUsageToday;
	public final int WattHoursUsageYesterday;
	public final int WattHoursUsageThisMonth;
	public final int WattHoursUsageThisYear;
	public final int WattHoursUsageTotal;
	public final int InstalledCapacity;
	
	private final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm:ss");
	
	public DataInstant(String json) {
		// Parse timestamp
		String timeAsString = json.substring(22,39);
		timestamp = LocalDateTime.parse(timeAsString, format);

		// Parse data
		String dataAsString = json.substring(41,json.length()-3);
		String[] parts = dataAsString.split(",");
		int[] data = new int[16];
		for (int i = 0; i < 16; i++) {
			String[] subparts = parts[i].split(":");
			data[i] = Integer.parseInt(subparts[1]);
		}
		PowerAC = data[0];
		PowerDC = data[1];
		VoltageAC = data[2];
		VoltageDC = data[3];
		WattHoursToday = data[4];
		WattHoursYesterday = data[5];
		WattHoursThisMonth = data[6];
		WattHoursThisYear = data[7];
		WattHoursTotal = data[8];
		WattsUsage = data[9];
		WattHoursUsageToday = data[10];
		WattHoursUsageYesterday = data[11];
		WattHoursUsageThisMonth = data[12];
		WattHoursUsageThisYear = data[13];
		WattHoursUsageTotal = data[14];
		InstalledCapacity = data[15];
	}
	
	@Override
	public String toString() {
		return "Timestamp: " + timestamp.format(format) +
				"\n" + "Power AC: " + PowerAC + "\n" + "Watts Usage: " + WattsUsage + "\n\n";
	}
}
