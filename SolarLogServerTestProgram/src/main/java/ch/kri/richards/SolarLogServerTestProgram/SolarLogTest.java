package ch.kri.richards.SolarLogServerTestProgram;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This example represents a very simple browser. We correctly fetch web-pages using
 * the HTTP/1.0 protocol. However, these pages are simply displayed in raw HTML
 */
public class SolarLogTest extends Application {
    private SolarLogTestView view;
    private SolarLogTestController controller;
    private SolarLogTestModel model;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Create the standard MVC pattern
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Initialize the GUI
        model = new SolarLogTestModel();
        view = new SolarLogTestView(primaryStage, model);
        controller = new SolarLogTestController(model, view);

        // Display the GUI after all initialization is complete
        view.start();
    }
}
