package ch.kri.richards.SolarLogServerTestProgram;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class SolarLogTestView {
    private SolarLogTestModel model;
    private Stage stage;

    protected Label lblIP = new Label("IP");
    protected TextField txtIP = new TextField("192.168.1.6");
    protected Label lblPort = new Label("Port");
    protected TextField txtPort = new TextField("80");
    protected Button btnGo = new Button("Go");
    protected TextArea txtInstantData = new TextArea();
    protected TextArea txtDeviceData = new TextArea();
    
    protected SolarLogTestView(Stage stage, SolarLogTestModel model) {
        this.stage = stage;
        this.model = model;
        
        BorderPane root = new BorderPane();

        HBox topBox = new HBox();
        topBox.setId("TopBox");
        Region spacer1 = new Region();
        Region spacer2 = new Region();
        HBox.setHgrow(spacer1, Priority.ALWAYS);
        HBox.setHgrow(spacer2, Priority.ALWAYS);
        root.setTop(topBox);
        
        topBox.getChildren().addAll(lblIP, txtIP, spacer1, lblPort, txtPort, spacer2, btnGo);
        txtIP.setId("IP");
        txtPort.setId("Port");
        
        HBox centerBox = new HBox();
        root.setCenter(centerBox);
        
        ScrollPane spLeft = new ScrollPane();
        spLeft.setHbarPolicy(ScrollBarPolicy.NEVER);
        spLeft.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        spLeft.setFitToHeight(true);
        spLeft.setFitToWidth(true);
        spLeft.setContent(txtInstantData);
        txtInstantData.setWrapText(true);
        centerBox.getChildren().add(spLeft);

        ScrollPane spRight = new ScrollPane();
        spRight.setHbarPolicy(ScrollBarPolicy.NEVER);
        spRight.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        spRight.setFitToHeight(true);
        spRight.setFitToWidth(true);
        spRight.setContent(txtDeviceData);
        txtDeviceData.setWrapText(true);
        centerBox.getChildren().add(spRight);
        
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add(
                getClass().getResource("SolarLogTest.css").toExternalForm());
        stage.setTitle("SolarLog Test");
        stage.setScene(scene);;
    }
    
    public void start() {
        stage.show();
    }
}
