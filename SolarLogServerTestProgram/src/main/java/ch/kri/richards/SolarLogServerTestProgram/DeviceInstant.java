package ch.kri.richards.SolarLogServerTestProgram;

import java.util.HashMap;
import java.util.Map;

/**
 * Query devices using "{"782":null}"
 * 
 * The devices are currently in the order:
 * 0 - Zähler
 * 1 - Schalter
 * 2 - Wechselrichter
 * 3 - Ladestation
 * 
 * This query does not provide a timestamp. We could take this from the HTTP headers, if needed.
 */
public class DeviceInstant {
	public final Map<Integer, Integer> deviceMapping = new HashMap<>();
	
	public DeviceInstant(String json) {
		String[] parts = json.split("\\{");
		parts = parts[2].split("\\}");
		parts = parts[0].split(",");
		
		for (String part : parts) {
			String[] devInfo = part.split(":");
			Integer id = Integer.parseInt(devInfo[0].substring(1, devInfo[0].length()-1));
			Integer value = Integer.parseInt(devInfo[1].substring(1, devInfo[1].length()-1));
			deviceMapping.put(id, value);
		}
	}
	
	@Override
	public String toString() {
		return "Counter: " + deviceMapping.get(0) + "\n" + "Inverter: " + deviceMapping.get(2) + "\n\n\n";
	}
}
