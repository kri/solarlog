package ch.kri.richards.SolarLogServerTestProgram;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javafx.beans.property.SimpleStringProperty;

public class SolarLogTestModel {
	public final SimpleStringProperty instantString = new SimpleStringProperty();
	public final SimpleStringProperty deviceString = new SimpleStringProperty();

	private DataInstant dataInstant;
	private DeviceInstant deviceInstant;

	public void browse(String ipAddress, Integer port) {
		getInstantData(ipAddress, port);
		getDeviceData(ipAddress, port);
	}

	public void getInstantData(String ipAddress, Integer port) {
		StringBuffer pageContent = new StringBuffer();
		Runnable gdd = () -> {
			while (true) {
				String lineIn;
				String json = null;
				try (Socket s = new Socket(ipAddress, port);
						OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
						BufferedReader inReader = new BufferedReader(new InputStreamReader(s.getInputStream()));) {
					// Read device data
					// Query current values
					String jsonQuery = "{\"801\":{\"170\":null}}";
					int len = jsonQuery.length();
					String jsonResponse = null;

					// Send our request, using the HTTP 1.0 protocol
					// Note: HTTP specifies \r\n line endings, though most programs don't care
					out.write("POST /getjp HTTP/1.1\r\n");
					out.write("Host: " + ipAddress + "\r\n");
					out.write("Content-Length: " + len + "\r\n\r\n"); // Blank line at the end of headers!!!
					out.write(jsonQuery + "\r\n"); // Content of POST query
					out.flush();

//					pageContent.append(jsonQuery + "\n\n");

					// Start reading, looking for the single line of json output
					// Note that response is *very* slow: 60 seconds
					while ((lineIn = inReader.readLine()) != null) {
//						pageContent.append(lineIn + "\n");
						if (jsonResponse == null && !lineIn.isEmpty() && lineIn.charAt(0) == '{') {
							jsonResponse = lineIn;
						}
					}
					dataInstant = new DataInstant(jsonResponse);
					pageContent.append(dataInstant.toString());
				}

				// If an error occurred, show the error message in txtInhalt
				catch (Exception err) {
					pageContent.append("ERROR: " + err.toString());
				}
				instantString.setValue(pageContent.toString());
			}
		};
		Thread t = new Thread(gdd);
		t.start();
	}

	public void getDeviceData(String ipAddress, Integer port) {
		StringBuffer pageContent = new StringBuffer();
		Runnable gdd = () -> {
			while (true) {
				String lineIn;
				String json = null;
				try (Socket s = new Socket(ipAddress, port);
						OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
						BufferedReader inReader = new BufferedReader(new InputStreamReader(s.getInputStream()));) {
					// Read device data
					String jsonQuery = "{\"782\":null}";
					int len = jsonQuery.length();
					String jsonResponse = null;

					// Send our request, using the HTTP 1.0 protocol
					// Note: HTTP specifies \r\n line endings, though most programs don't care
					out.write("POST /getjp HTTP/1.1\r\n");
					out.write("Host: " + ipAddress + "\r\n");
					out.write("Content-Length: " + len + "\r\n\r\n"); // Blank line at the end of headers!!!
					out.write(jsonQuery + "\r\n"); // Content of POST query
					out.flush();

//					pageContent.append(jsonQuery + "\n\n");

					// Start reading, looking for the single line of json output
					// Note that response is *very* slow: 60 seconds
					while ((lineIn = inReader.readLine()) != null) {
						pageContent.append(lineIn + "\n");
						if (jsonResponse == null && !lineIn.isEmpty() && lineIn.charAt(0) == '{') {
							jsonResponse = lineIn;
						}
					}
					deviceInstant = new DeviceInstant(jsonResponse);
					pageContent.append(deviceInstant.toString());
				}

				// If an error occurred, show the error message in txtInhalt
				catch (Exception err) {
					pageContent.append("ERROR: " + err.toString());
				}
				deviceString.setValue(pageContent.toString());
			}
		};
		Thread t = new Thread(gdd);
		t.start();
	}
}
