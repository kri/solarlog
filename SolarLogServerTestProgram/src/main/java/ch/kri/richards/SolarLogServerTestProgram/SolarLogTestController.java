package ch.kri.richards.SolarLogServerTestProgram;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;

public class SolarLogTestController {
	final private SolarLogTestModel model;
	final private SolarLogTestView view;

	private final SimpleIntegerProperty counter = new SimpleIntegerProperty();

	protected SolarLogTestController(SolarLogTestModel model, SolarLogTestView view) {
		this.model = model;
		this.view = view;

		model.instantString.addListener((o, oldValue, newValue) -> {
			Platform.runLater(() -> {
				view.txtInstantData.setText(model.instantString.getValue());
			});
		});
		model.deviceString.addListener((o, oldValue, newValue) -> {
			Platform.runLater(() -> {
				view.txtDeviceData.setText(model.deviceString.getValue());
			});
		});

		counter.addListener((o, oldValue, newValue) -> {
			Platform.runLater(() -> {
				view.btnGo.setText(newValue.toString());
			});
		});

		// register ourselves to listen for button clicks
		view.btnGo.setOnAction(e -> {
			String ipAddress = view.txtIP.getText();
			Integer port = Integer.parseInt(view.txtPort.getText());
			model.browse(ipAddress, port);
			count();
			view.btnGo.setDisable(true);
		});
	}

	private void count() {
		Runnable count = () -> {
			while (true) {
				counter.setValue(counter.getValue() + 1);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		};
		Thread t = new Thread(count);
		t.start();
	}
}
